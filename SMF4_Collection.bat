@echo off
echo Exchange Data International Limited - SMF4 Collections
echo.
echo.
echo Check SMF morning load is OK on web before continuing
rem O:\apps\url\web_db_check.url

echo Tidy SMF4 processing directory before proceding
@rem o:\scripts\^tidysmf4.bat
echo Get SMF 1 daily feed from LSE
@rem C:\Progra~1\GlobalSCAPE\CuteFTP\^cutftp32.exe^MACRO=o:\scripts\getsmf1.scr
echo Unpack SMF4 daily changes
@rem o:\scripts\^smfunpak.bat
exit
